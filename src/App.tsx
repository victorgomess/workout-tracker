import { Outlet } from "react-router-dom";
import Header from "./components/Header";
import LinkButton from "./components/buttons/LinkButton";

function App() {
  return (
    <div className="App sm:container mx-auto px-4">
      <Header />

      <main>
        {/* <div className="cta flex columns-1 justify-center my-6">
          <LinkButton to="workout" title="Start New Workout" />
        </div> */}

        {/* <div className="flex columns-auto justify-between gap-x-4">
          <LinkButton to="/history" title="History" />
          <LinkButton to="/timer" title="Timer" />
          <LinkButton to="/add-workout" title="Add Workout Plan" />
        </div> */}
      </main>

      <Outlet />
    </div>
  );
}

export default App;
