import React from "react";
import { useRouteError } from "react-router-dom";
import { errorMessage } from "./util/common";
import { Link } from "react-router-dom";

const ErrorPage: React.FC = () => {
  const error = useRouteError();

  return (
    <div className="flex flex-col container mx-auto py-8 flex items-center justify-center h-screen gap-y-8">
      <div className="content text-center leading-10 space-y-6">
        <h1 className="text-8xl font-bold center">Ooops! 😅</h1>
        <p className="text-4xl">Sorry, an unexpected error occurred :(</p>
        <p className="text-xl font-thin">
          <i>{errorMessage(error)}</i>
        </p>
      </div>

      <div className="footer">
        <Link className="underline" to="/" replace={true}>
          Return to Home
        </Link>
      </div>
    </div>
  );
};

export default ErrorPage;
