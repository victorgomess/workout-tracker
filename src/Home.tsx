import { Outlet } from "react-router-dom";
import LinkButton from "./components/buttons/LinkButton";
import Container from "./components/Container";

function Home() {
  return (
    <Container>
      <div className="cta flex columns-1 justify-center my-6">
        <LinkButton to="/workouts" title="Start New Workout" />
      </div>

      <Outlet />
    </Container>
  );
}

export default Home;
