import React from "react";

const Container: React.FC<React.PropsWithChildren> = ({ children }) => {
  return <div className="sm:container mx-auto px-4">{children}</div>;
};

export default Container;
