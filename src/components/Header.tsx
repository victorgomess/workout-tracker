import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { canGoBack, goBack } from "../util/navigation";

const Header: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();

  return (
    <header id="header mb-8">
      <div className="container py-6 mx-auto flex gap-4">
        {canGoBack(location) && (
          <button className="go-back" onClick={goBack.bind(this, navigate)}>
            {"<"}
          </button>
        )}
        <h1 className="text-3xl font-bold">Workout Tracker App</h1>
      </div>
    </header>
  );
};

export default Header;
