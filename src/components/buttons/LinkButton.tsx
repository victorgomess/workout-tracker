import React from "react";
import { Link } from "react-router-dom";

interface ILinkButton {
  title: string;
  to: string;
}

const LinkButton: React.FC<ILinkButton> = ({ title, to }) => {
  return (
    <Link
      to={to}
      className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
    >
      {title}
    </Link>
  );
};

export default LinkButton;
