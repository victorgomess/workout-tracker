import { createBrowserRouter } from "react-router-dom";
import App from "./App";
import ErrorPage from "./ErrorPage";
import WorkoutPage from "./workout/WorkoutPage";
import Home from "./Home";
import Container from "./components/Container";

const router = createBrowserRouter([
  {
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Home />,
        errorElement: <ErrorPage />,
        index: true,
      },
      {
        path: "/workouts",
        element: <WorkoutPage />,
        errorElement: <ErrorPage />,
      },
    ],
  },
  {
    path: "/workout/session/:id?",
    element: (
      <Container>
        <h1>Workout Session</h1>
      </Container>
    ),
    errorElement: <ErrorPage />,
  },
]);

export default router;
