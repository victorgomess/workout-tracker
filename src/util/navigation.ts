import { NavigateFunction, Location } from "react-router-dom";

export const goBack = (navigate: NavigateFunction) => {
  navigate(-1);
};

export const canGoBack = (location: Location) => {
  return location.key !== "default" && location.pathname !== "/";
};
