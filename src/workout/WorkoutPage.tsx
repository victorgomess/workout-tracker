import React from "react";
import LinkButton from "../components/buttons/LinkButton";
import Container from "../components/Container";

const WorkoutPage: React.FC = () => {
  return (
    <Container>
      <div className="flex flex-col text-center">
        <LinkButton to="/workout/session" title="Start new Workout" />
      </div>

      <hr className="my-8" />

      <h2 className="text-xl">Choose a workout</h2>

      <div className="flex flex-col gap-y-4 py-4">
        <LinkButton to="/workouts/1" title="Workout 1" />
        <LinkButton to="/workouts/2" title="Workout 2" />
        <LinkButton to="/workouts/3" title="Workout 3" />
      </div>
    </Container>
  );
};

export default WorkoutPage;
